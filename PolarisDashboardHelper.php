<?php

class PolarisDashboardHelper
{
    private $baseUrl = "https://polaris.digitallearning.gmbh";

    private $application_token;

    public function __construct($application_token ,$baseUrl = null)
    {
        $this->application_token = $application_token;
        if($baseUrl != null)
            $this->baseUrl = $baseUrl;
    }

    public function fetchVisualisationToken($engines = [], $context_id = null, $engines_with_context = [])
    {
        if($context_id != null) {
            $response = $this->postRequest("/api/v1/provider/visualization-tokens/create",
                json_encode(["engines" => $engines, "context_id" => $context_id, "engines_with_context" => $engines_with_context]));
        } else {
            $response = $this->postRequest("/api/v1/provider/visualization-tokens/create",
                json_encode(["engines" => $engines]));
        }
        return $response->token;
    }

    public function fetchDashboardResult($visualisation_token, $asDot = false)
    {
        $analytics_engine_result = $this->getRequest("/api/v1/provider/result",$visualisation_token);
        if($asDot)
        {
            $ritit = new RecursiveIteratorIterator(new RecursiveArrayIterator($analytics_engine_result));
            $result = array();
            foreach ($ritit as $leafValue) {
                $keys = array();
                foreach (range(0, $ritit->getDepth()) as $depth) {
                    $keys[] = $ritit->getSubIterator($depth)->key();
                }
                $result[ join('.', $keys) ] = $leafValue;
            }
            return $result;
        }
        return $analytics_engine_result;
    }

    function getRequest($url,$visualisation_token)
    {
        $curlHandle = curl_init($this->baseUrl.$url);
        $header = array();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: Basic '.$visualisation_token;

        curl_setopt($curlHandle, CURLOPT_HTTPHEADER,$header);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        $curlResponse = curl_exec($curlHandle);
        if (curl_errno($curlHandle)) {
            $error_msg = curl_error($curlHandle);
        }
        if (isset($error_msg)) {
            print_r($curlResponse);
            print_r($error_msg);
        }
        curl_close($curlHandle);
        return json_decode($curlResponse);
    }

    function postRequest($url,$data)
    {
        $curlHandle = curl_init($this->baseUrl.$url);
        $header = array();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: Basic '.$this->application_token;

        curl_setopt($curlHandle, CURLOPT_HTTPHEADER,$header);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        $curlResponse = curl_exec($curlHandle);
        if (curl_errno($curlHandle)) {
            $error_msg = curl_error($curlHandle);
        }
        if (isset($error_msg)) {
            print_r($curlResponse);
            print_r($error_msg);
        }
        curl_close($curlHandle);
        return json_decode($curlResponse);
    }
}