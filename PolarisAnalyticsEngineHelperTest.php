<?php

require "PolarisAnalyticsEngineHelper.php";
class PolarisAnalyticsEngineHelperTest
{
    private $helper;

    public function __construct($analytics_token)
    {
        $this->helper = new PolarisAnalyticsEngineHelper($analytics_token);
    }

    public function testGetStatements()
    {
        $statements = $this->helper->getStatements();
        print_r("All statements \n\r");
        print_r($statements);
    }

    public function testSaveResults($user)
    {
        $result = [
            "current_time" => strtotime("now"),
            "user" => $user // only for test
        ];
        $resultResponse = $this->helper->saveResult($result,"test description",$user);
        print_r($resultResponse);
    }

    public function testGetResults()
    {
       $existing_result = $this->helper->getExistingResult();
       print_r($existing_result);
    }
}

$test = new PolarisAnalyticsEngineHelperTest("0838b91a5d71822ce87bab1de5dd65c52add4865dd2bbcb372604163940285c4");
$test->testGetStatements();

$test->testSaveResults("user1@polaris.com");
$test->testSaveResults("user2@polaris.com");
$test->testSaveResults("user3@polaris.com");

$test->testGetResults();