<?php

class PolarisAnalyticsEngineHelper
{
    private $page_size = 100;
    private $baseUrl = "https://polaris.digitallearning.gmbh";

    private $analytics_token;

    public function __construct($analytics_token, $baseUrl = null)
    {
        $this->analytics_token = $analytics_token;
        if ($baseUrl != null)
            $this->baseUrl = $baseUrl;
    }

    public function getStatementsPage($last_object_id = null)
    {
        if ($last_object_id)
            $payload = [
                "page_size" => $this->page_size,
                "last_object_id" => $last_object_id,
            ];
        else
            $payload = [
                "page_size" => $this->page_size,
            ];

        $response = $this->postRequest("/api/v1/provider/data",json_encode($payload));
        return $response->statements;
    }

    public function getStatements()
    {
        $statements = array();
        $read_all_pages = false;
        $last_object_id = null;
        while (!$read_all_pages) {
            $page_statements = $this->getStatementsPage($last_object_id);
            $statements = array_merge($statements, $page_statements);
            if (count($page_statements) == 0) {
                $read_all_pages = true;
            } else {
                $last_object_id = $statements[-1]["_id"];
            }
        }
        return $statements;
    }

    public function getExistingResult()
    {
        return $this->getRequest("/api/v1/provider/results");
    }

    public function saveResult($result, $description = null, $context_id = null)
    {
        $payload = [];
        $payload["result"] = $result;
        if($description)
        {
            $payload["description"] = $description;
        }
        if($context_id)
        {
            $payload["context_id"] = $context_id;
        }
        return $this->postRequest("/api/v1/provider/store-result",json_encode($payload));
    }

    function getRequest($url)
    {
        $curlHandle = curl_init($this->baseUrl . $url);
        $header = array();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: Basic ' . $this->analytics_token;

        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        $curlResponse = curl_exec($curlHandle);
        if (curl_errno($curlHandle)) {
            $error_msg = curl_error($curlHandle);
        }
        if (isset($error_msg)) {
            print_r($curlResponse);
            print_r($error_msg);
        }
        curl_close($curlHandle);
        return json_decode($curlResponse);
    }

    function postRequest($url, $data)
    {
        $curlHandle = curl_init($this->baseUrl . $url);
        $header = array();
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: Basic ' . $this->analytics_token;

        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_FAILONERROR, true);

        $curlResponse = curl_exec($curlHandle);
        if (curl_errno($curlHandle)) {
            $error_msg = curl_error($curlHandle);
        }
        if (isset($error_msg)) {
            print_r($curlResponse);
            print_r($error_msg);
        }
        curl_close($curlHandle);
        return json_decode($curlResponse);
    }
}