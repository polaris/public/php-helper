<?php

require "PolarisDashboardHelper.php";

class PolarisDashboardHelperTest
{
    private $helper;

    public function __construct($token)
    {
        $this->helper = new PolarisDashboardHelper($token);
    }

    public function testFetchVisualisationToken()
    {
        $visualisationToken = $this->helper->fetchVisualisationToken(["test-engine"]);
        //print_r("Token: ".$visualisationToken);
        return $visualisationToken;
    }

    public function testFetchAnalyticsResult($visualisationToken)
    {
        $analytics_engine_result = $this->helper->fetchDashboardResult($visualisationToken,true);
        print_r($analytics_engine_result);
    }

    public function testFetchVisualisationTokenWithContext($user)
    {
        $visualisationToken = $this->helper->fetchVisualisationToken([],$user,["test-engine"]);
        //print_r("Token: ".$visualisationToken);
        return $visualisationToken;
    }
}


$test = new PolarisDashboardHelperTest("27dd35314d73c7618945af9ac3152ecfcb9d1992ead2bdc562409d01ed456f6d");
$visualisationToken = $test->testFetchVisualisationToken();
$test->testFetchAnalyticsResult($visualisationToken);


$visualisationToken = $test->testFetchVisualisationTokenWithContext("user2@polaris.com");
$test->testFetchAnalyticsResult($visualisationToken);